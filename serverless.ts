import type { AWS } from "@serverless/typescript"

// DynamoDB
import dynamoDBTables from "./resources/dynamodb"

import {
    getSlackUser,
    getTeamsUser,
    getAllUsers,
    createUser,
    updateUser
} from "@functions/user"

const serverlessConfiguration: AWS = {
    service: "aws-serverless-typescript-api",
    frameworkVersion: "3",
    plugins: ["serverless-esbuild", "serverless-offline", "serverless-dynamodb-local"],
    provider: {
        name: "aws",
        runtime: "nodejs14.x",
        region: "us-east-1",
        apiGateway: {
            minimumCompressionSize: 1024,
            shouldStartNameWithService: true
        },
        environment: {
            AWS_NODEJS_CONNECTION_REUSE_ENABLED: "1",
            USER_TABLE: "UsersTable",
            NODE_OPTIONS: "--enable-source-maps --stack-trace-limit=1000"
        },
        iam: {
            role: {
                statements: [
                    {
                        Effect: "Allow",
                        Action: [
                            "dynamodb:DescribeTable",
                            "dynamodb:Query",
                            "dynamodb:Scan",
                            "dynamodb:GetItem",
                            "dynamodb:PutItem",
                            "dynamodb:BatchWriteItem",
                            "dynamodb:UpdateItem",
                            "dynamodb:DeleteItem"
                        ],
                        Resource:
                            "arn:aws:dynamodb:${self:provider.region}:*:table/${self:provider.environment.USER_TABLE}"
                    }
                ]
            }
        }
    },
    // import the function via paths
    functions: {
        getSlackUser,
        getTeamsUser,
        getAllUsers,
        createUser,
        updateUser
    },
    package: { individually: true },
    custom: {
        esbuild: {
            bundle: true,
            minify: false,
            sourcemap: true,
            exclude: ["aws-sdk"],
            target: "node14",
            define: { "require.resolve": undefined },
            platform: "node",
            concurrency: 10
        },
        dynamodb: {
            start: {
                port: 5000,
                inMemory: true,
                migrate: true
            },
            stages: "dev"
        }
    },
    resources: {
        Resources: dynamoDBTables
    }
}

module.exports = serverlessConfiguration
