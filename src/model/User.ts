export default interface User {
    PK: string
    SK: string
    email?: string
    subscriptionActive?: boolean
    slackId?: string
    teamsId?: string
}
