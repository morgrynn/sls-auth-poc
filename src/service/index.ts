import dynamoDBClient from "../model";
import UserService from "./userService"

const { USER_TABLE } = process.env

const userService = new UserService(dynamoDBClient(), USER_TABLE);
export default userService;