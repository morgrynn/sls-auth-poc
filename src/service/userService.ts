import { DocumentClient } from "aws-sdk/clients/dynamodb"
import User from "../model/User"

export enum facets {
    userFacet = "USER#",
    slackFacet = "SLACK#",
    teamsFacet = "TEAMS#"
}

export default class Service {
    constructor(private readonly docClient: DocumentClient, private readonly tableName: string) {}

    async getAllUsers(): Promise<User[]> {
        const users = await this.docClient
            .scan({
                TableName: this.tableName
            })
            .promise()
        return users.Items as User[]
    }

    async getSlackUser(slackId: string): Promise<User> {
        try {
            const user = await this.docClient
                .get({
                    TableName: this.tableName,
                    Key: {
                        PK: `SLACK#${slackId}`,
                        SK: `SLACK#${slackId}`
                    }
                })
                .promise()
            if (!user.Item) {
                throw new Error("SlackId does not exit")
            }
            return user.Item as User
        } catch (error) {
            console.log(error)
            throw error
        }
    }

    async getTeamsUser(teamsId: string): Promise<User> {
        try {
            const user = await this.docClient
                .get({
                    TableName: this.tableName,
                    Key: {
                        PK: `TEAMS#${teamsId}`,
                        SK: `TEAMS#${teamsId}`
                    }
                })
                .promise()
            if (!user.Item) {
                throw new Error("TeamsId does not exit")
            }
            return user.Item as User
        } catch (error) {
            console.log(error)
            throw error
        }
    }

    async createUser(user: User): Promise<User> {
        try {
            await this.docClient
                .batchWrite({
                    RequestItems: {
                        [this.tableName]: [
                            {
                                PutRequest: {
                                    Item: user
                                },
                                
                            },
                            {
                                PutRequest: {
                                    Item: {
                                        PK: `${facets.slackFacet}${user.slackId}`,
                                        SK: `${facets.slackFacet}${user.slackId}`,
                                        subscriptionActive: user.subscriptionActive,
                                        slackId: user.slackId
                                    }
                                }
                            },
                            {
                                PutRequest: {
                                    Item: {
                                        PK: `${facets.teamsFacet}${user.teamsId}`,
                                        SK: `${facets.teamsFacet}${user.teamsId}`,
                                        subscriptionActive: user.subscriptionActive,
                                        teamsId: user.teamsId
                                    }
                                }
                            },
                            {
                                PutRequest: {
                                    Item: {
                                        PK: `${facets.userFacet}${user.email}`,
                                        SK: `${facets.slackFacet}${user.slackId}`,
                                        subscriptionActive: user.subscriptionActive,
                                        slackId: user.slackId
                                    }
                                }
                            },
                            {
                                PutRequest: {
                                    Item: {
                                        PK: `${facets.userFacet}${user.email}`,
                                        SK: `${facets.teamsFacet}${user.teamsId}`,
                                        subscriptionActive: user.subscriptionActive,
                                        teamsId: user.teamsId
                                    }
                                }
                            }
                        ]
                    }
                })
                .promise()
            return user as User
        } catch (error) {
            console.log(error)
            throw error
        }
    }

    async getUser(email: any): Promise<User> {
        try {
            const user = await this.docClient
                .get({
                    TableName: this.tableName,
                    Key: {
                        PK: `${facets.userFacet}${email}`,
                        SK: `${facets.userFacet}${email}`
                    }
                })
                .promise()
            if (!user.Item) {
                throw new Error("User does not exit")
            }
            return user.Item as User
        } catch (error) {
            console.log(error)
            throw error
        }
    }

    async updateUser(email: string, user: Partial<User>, currentUser: User): Promise<User> {
        try {
            await this.docClient
                .transactWrite({
                    TransactItems: [
                        {
                            Update: {
                                TableName: this.tableName,
                                Key: {
                                    PK: `${facets.userFacet}${email}`,
                                    SK: `${facets.userFacet}${email}`
                                },
                                UpdateExpression: `set subscriptionActive = :subscriptionActive`,
                                ExpressionAttributeValues: {
                                    ":subscriptionActive": user.subscriptionActive
                                }
                            }
                        },
                        {
                            Update: {
                                TableName: this.tableName,
                                Key: {
                                    PK: `${facets.userFacet}${email}`,
                                    SK: `${facets.slackFacet}${currentUser.slackId}`
                                },
                                UpdateExpression: `set subscriptionActive = :subscriptionActive`,
                                ExpressionAttributeValues: {
                                    ":subscriptionActive": user.subscriptionActive
                                }
                            }
                        },
                        {
                            Update: {
                                TableName: this.tableName,
                                Key: {
                                    PK: `${facets.userFacet}${currentUser.email}`,
                                    SK: `${facets.teamsFacet}${currentUser.teamsId}`
                                },
                                UpdateExpression: `set subscriptionActive = :subscriptionActive`,
                                ExpressionAttributeValues: {
                                    ":subscriptionActive": user.subscriptionActive
                                }
                            }
                        },
                        {
                            Update: {
                                TableName: this.tableName,
                                Key: {
                                    PK: `${facets.slackFacet}${currentUser.slackId}`,
                                    SK: `${facets.slackFacet}${currentUser.slackId}`
                                },
                                UpdateExpression: `set subscriptionActive = :subscriptionActive`,
                                ExpressionAttributeValues: {
                                    ":subscriptionActive": user.subscriptionActive
                                }
                            }
                        },
                        {
                            Update: {
                                TableName: this.tableName,
                                Key: {
                                    PK: `${facets.teamsFacet}${currentUser.teamsId}`,
                                    SK: `${facets.teamsFacet}${currentUser.teamsId}`
                                },
                                UpdateExpression: `set subscriptionActive = :subscriptionActive`,
                                ExpressionAttributeValues: {
                                    ":subscriptionActive": user.subscriptionActive
                                }
                            }
                        }
                    ]
                })
                .promise()
            return user as User
        } catch (error) {
            console.log(error)
            throw error
        }
    }
}
