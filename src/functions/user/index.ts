import { handlerPath } from "@libs/handler-resolver"

export const getAllUsers = {
    handler: `${handlerPath(__dirname)}/handler.getAllUsers`,
    events: [
        {
            http: {
                method: "get",
                path: "user/"
            }
        }
    ]
}

export const getSlackUser = {
    handler: `${handlerPath(__dirname)}/handler.getSlackUser`,
    events: [
        {
            http: {
                method: "get",
                path: "slack/{slackId}"
            }
        }
    ]
}

export const getTeamsUser = {
    handler: `${handlerPath(__dirname)}/handler.getTeamsUser`,
    events: [
        {
            http: {
                method: "get",
                path: "teams/{teamsId}"
            }
        }
    ]
}

export const createUser = {
    handler: `${handlerPath(__dirname)}/handler.createUser`,
    events: [
        {
            http: {
                method: "post",
                path: "user"
            }
        }
    ]
}


export const updateUser = {
    handler: `${handlerPath(__dirname)}/handler.updateUser`,
    events: [
        {
            http: {
                method: "put",
                path: "user/{email}"
            }
        }
    ]
}


