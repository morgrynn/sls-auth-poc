import { APIGatewayProxyEvent, APIGatewayProxyResult } from "aws-lambda"
import { formatJSONResponse } from "@libs/api-gateway"
import { middyfy } from "@libs/lambda"
import userService from "../../service"
import { facets } from "../../service/userService"

export const getAllUsers = middyfy(async (): Promise<APIGatewayProxyResult> => {
    try {
        const users = await userService.getAllUsers()
        return formatJSONResponse({
            users
        })
    } catch (e) {
        return formatJSONResponse({
            status: 500,
            message: e
        })
    }
})

export const getSlackUser = middyfy(async (event: APIGatewayProxyEvent): Promise<APIGatewayProxyResult> => {
    const slackId = event.pathParameters.slackId
    try {
        const user = await userService.getSlackUser(slackId)
        return formatJSONResponse({
            user
        })
    } catch (e) {
        return formatJSONResponse({
            status: 500,
            message: e
        })
    }
})

export const getTeamsUser = middyfy(async (event: APIGatewayProxyEvent): Promise<APIGatewayProxyResult> => {
    const teamsId = event.pathParameters.teamsId
    try {
        const user = await userService.getTeamsUser(teamsId)
        return formatJSONResponse({
            user
        })
    } catch (e) {
        return formatJSONResponse({
            status: 500,
            message: e
        })
    }
})

export const createUser = middyfy(async (event: APIGatewayProxyEvent): Promise<APIGatewayProxyResult> => {
    const { subscriptionActive, teamsId, email, slackId } = event.body as any
    try {
        const user = await userService.createUser({
            email: email,
            subscriptionActive: subscriptionActive,
            slackId: slackId,
            teamsId: teamsId,
            PK: `${facets.userFacet}${email}`,
            SK: `${facets.userFacet}${email}`
        })
        return formatJSONResponse({
            user
        })
    } catch (e) {
        return formatJSONResponse({
            status: 500,
            message: e
        })
    }
})

export const updateUser = middyfy(async (event: APIGatewayProxyEvent): Promise<APIGatewayProxyResult> => {
    const { email } = event.pathParameters as any
    const { subscriptionActive } = event.body as any

    try {
        const currentUser = await userService.getUser(email)

        const user = await userService.updateUser(email, { subscriptionActive }, currentUser)

        return formatJSONResponse({
            user
        })
    } catch (e) {
        return formatJSONResponse({
            status: 500,
            message: e
        })
    }
})
