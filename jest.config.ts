import type { Config } from "@jest/types"

const config: Config.InitialOptions = {
    // preset: "@shelf/jest-dynamodb",
    preset: "ts-jest",
    testEnvironment: "node",
    verbose: true,
    automock: true
}
export default config
